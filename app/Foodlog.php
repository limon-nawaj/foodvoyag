<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Foodlog extends Model
{
    protected $fillable = ['tinid', 'contact_address', 'contact_number', 'picture', 'nid', 'etinid', 'tradelicenseid'];
}
