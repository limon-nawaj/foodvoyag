<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Auditlog;
use PDF;


class CertificateController extends Controller
{
    public function pdfview(Request $request)
    {
    	$audilogs = Auditlog::all();
        view()->share('audilogs',$audilogs);

        if($request->has('download')){
            $pdf = PDF::loadView('certificate.index');
            return $pdf->download('certificate.pdf');
        }

        return view('certificate.index');
    }
}
