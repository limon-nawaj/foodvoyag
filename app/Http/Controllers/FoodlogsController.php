<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Foodlog;
use Image;

class FoodlogsController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('foodlogs.index');
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $foodlog = new Foodlog;

        $foodlog->tinid = $request->tinid;
        $foodlog->contact_address = $request->contact_address;
        $foodlog->contact_number = $request->contact_number;

        $picture = $request->file('picture');
        $picturename = time().'-'.$picture->getClientOriginalName();
        Image::make($picture->getRealPath())->save(public_path('uploads/foodlogs/pictures/'.$picturename));
        $foodlog->picture = $picturename;

        $nid = $request->file('nid');
        $nidname = time().'-'.$nid->getClientOriginalName();
        Image::make($nid->getRealPath())->save(public_path('uploads/foodlogs/nids/'.$nidname));
        $foodlog->nid = $nidname;

        $etinid = $request->file('etinid');
        $etinidname = time().'-'.$etinid->getClientOriginalName();
        Image::make($etinid->getRealPath())->save(public_path('uploads/foodlogs/etinids/'.$etinidname));
        $foodlog->etinid = $etinidname;

        $taxid = $request->file('taxid');
        $taxidname = time().'-'.$taxid->getClientOriginalName();
        Image::make($taxid->getRealPath())->save(public_path('uploads/foodlogs/taxids/'.$taxidname));
        $foodlog->taxid = $taxidname;

        $tradelicenseid = $request->file('tradelicenseid');
        $tradelicenseidname = time().'-'.$tradelicenseid->getClientOriginalName();
        Image::make($tradelicenseid->getRealPath())->save(public_path('uploads/foodlogs/tradelicenseids/'.$tradelicenseidname));
        $foodlog->tradelicenseid = $tradelicenseidname;


        $foodlog->save();

        return redirect(route('foodlogs.index'));
    }

    
}
