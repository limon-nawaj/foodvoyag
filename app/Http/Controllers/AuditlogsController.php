<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Auditlog;
use App\Foodlog;
class AuditlogsController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $foodlogs = Foodlog::all();
        return view('auditlogs.index')->with('foodlogs', $foodlogs);
    }

   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Auditlog::create($request->only('Flavor_and_Fragrance', 'Allergens', 
        'Chemical_and_nutritional_analysis', 'GMOs_Detection', 'Microbiological_Testing',
        'Residues_and_Contaminants_Testing', 'Sensory_Analysis', 'Analytical_Chemistry_Tests', 'Foodborne_Virus_Detection', 'Ingredients_and_Additives'));

        return redirect(route('auditlogs.index'));
    }

    
}
