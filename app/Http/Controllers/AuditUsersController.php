<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AuditUsersController extends Controller
{
    
    function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auditusers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User;

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->role = 'audit';

        $user->save();

        return redirect(route('auditusers.create'));
    }

    
}
