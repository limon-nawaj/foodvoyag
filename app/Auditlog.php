<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Auditlog extends Model
{
    protected $fillable = ['Flavor_and_Fragrance', 'Allergens', 
    	'Chemical_and_nutritional_analysis', 'GMOs_Detection', 'Microbiological_Testing',
    	'Residues_and_Contaminants_Testing', 'Sensory_Analysis', 'Analytical_Chemistry_Tests', 'Foodborne_Virus_Detection', 'Ingredients_and_Additives'];
}
