<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foodlogs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tinid');
            $table->string('contact_address');
            $table->string('contact_number');
            $table->string('picture'); //image type data
            $table->string('nid');     //image type data
            $table->string('etinid'); //image type data
            $table->string('taxid'); //image type data
            $table->string('tradelicenseid'); //image type data            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foodlogs');
    }
}
