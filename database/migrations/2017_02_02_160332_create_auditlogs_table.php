<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auditlogs', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('Flavor_and_Fragrance')->default(0)->nullable();
            $table->boolean('Allergens')->default(0)->nullable();
            $table->boolean('Chemical_and_nutritional_analysis')->default(0)->nullable();
            $table->boolean('GMOs_Detection')->default(0)->nullable();
            $table->boolean('Microbiological_Testing')->default(0)->nullable();
            $table->boolean('Residues_and_Contaminants_Testing')->default(0)->nullable();
            $table->boolean('Sensory_Analysis')->default(0)->nullable();
            $table->boolean('Analytical_Chemistry_Tests')->default(0)->nullable();
            $table->boolean('Foodborne_Virus_Detection')->default(0)->nullable();
            $table->boolean('Ingredients_and_Additives')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auditlogs');
    }
}
