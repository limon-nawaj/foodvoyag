<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'FoodlogsController@index');
Route::resource('foodlogs', 'FoodlogsController', ['only'=>['index', 'store']]);
Route::resource('auditlogs', 'AuditlogsController', ['only'=>['index', 'store']]);
Route::resource('auditusers', 'AuditUsersController', ['only'=>['create', 'store']]);

Route::get('certificate', 'CertificateController@pdfview')->name('certificate');

Auth::routes();

