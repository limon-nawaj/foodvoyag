@extends('layouts.app')

@section('content')
	
	<div class="col-md-6">
		<table class="table table-striped">
			<tr>
				<th>Tinid</th>
				<th>Contact Address</th>
				<th>Contact Number</th>
			</tr>
			@foreach ($foodlogs as $foodlog)
				<tr>
					<td>{{ $foodlog->tinid }}</td>
					<td>{{ $foodlog->contact_address }}</td>
					<td>{{ $foodlog->contact_number }}</td>
				</tr>
			@endforeach
		</table>
	</div>

	
	<div class="col-md-6">
		{!! Form::open(['route'=>'auditlogs.store']) !!}

			<div class="form-group">
				<label>
					{!! Form::checkbox('Flavor_and_Fragrance') !!} Flavor and Fragrance
				</label>
			</div>

		    <div class="form-group">
		      <label>
		        {!! Form::checkbox('Allergens') !!} Allergens
		      </label>
		    </div>

		    <div class="form-group">
		      <label>
		        {!! Form::checkbox('Chemical_and_nutritional_analysis') !!} Chemical and nutritional analysis
		      </label>
		    </div>


		    <div class="form-group">
		      <label>
		        {!! Form::checkbox('GMOs_Detection') !!} GMOs Detection
		      </label>
		    </div>

		    <div class="form-group">
		      <label>
		        {!! Form::checkbox('Microbiological_Testing') !!} Microbiological Testing
		      </label>
		    </div>

		    <div class="form-group">
		      <label>
		        {!! Form::checkbox('Residues_and_Contaminants_Testing') !!} Residues and Contaminants Testing
		      </label>
		    </div>

		    <div class="form-group">
		      <label>
		        {!! Form::checkbox('Sensory_Analysis') !!} Sensory Analysis
		      </label>
		    </div>

		    <div class="form-group">
		      <label>
		        {!! Form::checkbox('Analytical_Chemistry_Tests') !!} Analytical Chemistry Tests
		      </label>
		    </div>


		    <div class="form-group">
		      <label>
		        {!! Form::checkbox('Foodborne_Virus_Detection') !!} Foodborne Virus Detection
		      </label>
		    </div>

		    <div class="form-group">
		      <label>
		        {!! Form::checkbox('Ingredients_and_Additives') !!} Ingredients and Additives
		      </label>
		    </div>




			{!! Form::submit('Save', ['class'=>'btn btn-success']) !!}

		{!! Form::close() !!}
	</div>


@endsection