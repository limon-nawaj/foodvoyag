@extends('layouts.app')

@section('content')
	
	{!! Form::open(['route'=>'foodlogs.store', 'enctype' => 'multipart/form-data']) !!}

    <div class="form-group col-md-8 col-md-offset-4">
      {!! Form::label('tinid') !!}
      {!! Form::text('tinid', null, ['class'=>'form-control']) !!}
    </div>

    <div class="form-group col-md-8 col-md-offset-4">
      {!! Form::label('contact_address') !!}
      {!! Form::text('contact_address', null, ['class'=>'form-control']) !!}
    </div>

    <div class="form-group col-md-8 col-md-offset-4">
      {!! Form::label('contact_number') !!}
      {!! Form::text('contact_number', null, ['class'=>'form-control']) !!}
    </div>

    <div class="form-group col-md-8 col-md-offset-4">
      {!! Form::label('picture') !!}
      {!! Form::file('picture', null, ['class'=>'form-control']) !!}
    </div>

    <div class="form-group col-md-8 col-md-offset-4">
      {!! Form::label('nid') !!}
      {!! Form::file('nid', null, ['class'=>'form-control']) !!}
    </div>

    <div class="form-group col-md-8 col-md-offset-4">
      {!! Form::label('etinid') !!}
      {!! Form::file('etinid', null, ['class'=>'form-control']) !!}
    </div>

    <div class="form-group col-md-8 col-md-offset-4">
      {!! Form::label('taxid') !!}
      {!! Form::file('taxid', null, ['class'=>'form-control']) !!}
    </div>

    <div class="form-group col-md-8 col-md-offset-4">
      {!! Form::label('tradelicenseid') !!}
      {!! Form::file('tradelicenseid', null, ['class'=>'form-control']) !!}
    </div>

    
    <div class="form-group col-md-8 col-md-offset-4">
      {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
    </div>
  {!! Form::close() !!}

@stop