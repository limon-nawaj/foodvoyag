<style type="text/css">
	table td, table th{
		border:1px solid black;
	}
</style>
<div class="container">
	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus quaerat eligendi qui saepe aspernatur blanditiis, ratione totam veniam fuga libero commodi, magnam id unde debitis aliquam error eveniet incidunt ad.
	<br/>
	<a href="{{ route('certificate',['download'=>'pdf']) }}">Download PDF</a>

	@foreach ($audilogs as $audilog)
		{{ $audilog->Flavor_and_Fragrance ? 'Flavor_and_Fragrance' : '' }}
		{{ $audilog->Allergens ? 'Allergens' : '' }}
		{{ $audilog->Chemical_and_nutritional_analysis ? 'Chemical_and_nutritional_analysis' : '' }}
		{{ $audilog->GMOs_Detection ? 'GMOs_Detection' : '' }}
		{{ $audilog->Microbiological_Testing ? 'Microbiological_Testing' : '' }}
		{{ $audilog->Residues_and_Contaminants_Testing ? 'Residues_and_Contaminants_Testing' : '' }}
		{{ $audilog->Sensory_Analysis ? 'Sensory_Analysis' : '' }}
		{{ $audilog->Analytical_Chemistry_Tests ? 'Analytical_Chemistry_Tests' : '' }}
		{{ $audilog->Foodborne_Virus_Detection ? 'Foodborne_Virus_Detection' : '' }}
		{{ $audilog->Ingredients_and_Additives ? 'Ingredients_and_Additives' : '' }}
	@endforeach
</div>