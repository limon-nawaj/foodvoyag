@extends('layouts.app')

@section('content')
	{!! Form::open(['route'=>'auditusers.store']) !!}
		

		<div class="form-group">
			{!! Form::label('name') !!}
			{!! Form::text('name', null, ['class'=>'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('email') !!}
			{!! Form::text('email', null, ['class'=>'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('password') !!}
			{!! Form::password('password', ['class'=>'form-control']) !!}
		</div>

		{!! Form::submit('Save', ['class'=>'form-control']) !!}

	{!! Form::close() !!}
@stop